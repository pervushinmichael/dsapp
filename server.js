const express = require('express');
const mongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const app = express();
const port = 8000;

const uri = "mongodb+srv://DS-admin:12341@dscluster.ybh2u.mongodb.net/dscluster?retryWrites=true&w=majority";
const dbName = "DScluster";

app.use(bodyParser.urlencoded({extended:true})); 
app.use(bodyParser.json());

(async ()=>{
    let client;
  
    try {
      client = await mongoClient.connect(uri, {
        useUnifiedTopology: true,
        useNewUrlParser: true
      });
      console.log("Connected correctly to server");
  
      const db = client.db(dbName);
      require('./app/routes')(app, db);
  
      app.listen(port, () => {
        console.log('We are live on ' + port);
      });
    } catch (err) {
      console.log(err.stack);
    }
  })();
