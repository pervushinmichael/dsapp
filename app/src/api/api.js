import axios from 'axios';

export const getAllNotes = () => // получение всех записей
axios.post('http://localhost:8000/notes/all');

export const deleteNote = (id)=>
axios.delete('http://localhost:8000/notes/'+id);

export const updateNote = (id,title,body)=>
axios.put('http://localhost:8000/notes/'+id,{
    title:title,
    body:body
    });

export const createNote = (title,body)=>
    axios.post('http://localhost:8000/notes/create',{
        title:title,
        body:body
    });
