import React, {Component} from 'react';
import SockJS from 'sockjs-client';
import {getAllNotes, deleteNote, updateNote, createNote} from './api/api';
import {Table, Card, Button, Input} from 'antd'
import "antd/dist/antd.css";
import "./App.css";

class App extends Component{

    state = {
      time:new Date().toLocaleTimeString(),
      serverTime:'нет данных'
    }

    componentDidMount(){
      this.sock = new SockJS('http://127.0.0.1:9999/echo');
      this.sock.onopen = ()=>{
        console.log('open');
      };

      this.sock.onmessage = this.onMessage.bind(this);
      this.sock.onclose = ()=>{
        console.log('close');
      };
      setInterval(this.tick.bind(this),1000);
      this.getAll();
    }

    onMessage(e){
      if(e.data){
        this.setState({
          serverTime:e.data
        });
      }
    }

    tick(){
      this.setState({
        time:new Date().toLocaleTimeString()
      });
    }

    getAll(){
      getAllNotes().then(res => {
          console.log(res)
        this.setState({
                data: res.data
              })
          }).catch(err => console.log(err));
    }

    delNote(id){
      deleteNote(id).then(res=>{
        this.getAll()
      }).catch(err =>console.log(err));
    }

    updNote(id, title, body){
      updateNote(id,title,body).then(res=>{
        this.getAll()
      }).catch(err=>console.log(err));
    }

    addNote=(title,body)=>{
      createNote(title,body).then(res=>{
        this.getAll()
      }).catch(err=>console.log(err));
    }

    render(){
      const columns = [
        {
          title: '_id',
          dataIndex: '_id',
          key: '_id',
        },
        {
          title: 'title',
          dataIndex: 'title',
          key: 'title',
        },
        {
          title: 'text',
          dataIndex: 'text',
          key: 'text',
        }];
      return(
        <Card 
          title={'Notes application'}
          actions={[
          <h1>Время: {this.state.time}</h1>, 
          <h1>Серверное время: {this.state.serverTime}</h1>
          ]}
        >
          <Table columns={columns} dataSource={this.state.data} />
          <div className="wrapper">
            <div className="buttonBlock">
              <div>действия:</div>
              <Button className="btn-action" onClick={()=>{
                let inputs = document.getElementsByClassName("in");
                let btns = document.getElementsByClassName("inBtn");
                btns[0].style.display = "block";
                btns[1].style.display = "none";
                btns[2].style.display = "none";
                inputs[0].style.display = "none";
                inputs[1].style.display = "block";
                inputs[2].style.display = "block";
              }}>добавление</Button>
              <Button className="btn-action" onClick={()=>{
                let inputs = document.getElementsByClassName("in");
                let btns = document.getElementsByClassName("inBtn");
                btns[0].style.display = "none";
                btns[1].style.display = "block";
                btns[2].style.display = "none";
                inputs[0].style.display = "block";
                inputs[1].style.display = "block";
                inputs[2].style.display = "block";
              }}>обновление</Button>
              <Button className="btn-action" onClick={()=>{
                let inputs = document.getElementsByClassName("in");
                let btns = document.getElementsByClassName("inBtn");
                btns[0].style.display = "none";
                btns[1].style.display = "none";
                btns[2].style.display = "block";
                inputs[0].style.display = "block";
                inputs[1].style.display = "none";
                inputs[2].style.display = "none";
              }}>удаление</Button>
            </div>
            <div className="input">
              <Input className="in" placeholder="id"></Input>
              <Input className="in" placeholder="title"></Input>
              <Input className="in" placeholder="text"></Input>
              <Button className="inBtn" onClick={()=>{
                    let inputs = document.getElementsByClassName("in");
                    this.addNote(inputs[1].value, inputs[2].value);
                  }
                }>добавить
              </Button>
              <Button className="inBtn" onClick={()=>{
                    let inputs = document.getElementsByClassName("in");
                    this.updNote(inputs[0].value, inputs[1].value,inputs[2].value);
                  }
                }>обновить
              </Button>
              <Button className="inBtn" onClick={()=>{
                    let id = document.getElementsByClassName("in")[0];
                    this.delNote(id.value);
                  }
                }>удалить
              </Button>
            </div>
          </div>
        </Card>
      );
    }
} 

export default App;
